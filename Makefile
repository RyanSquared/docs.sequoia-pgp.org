# Source repository.
REPOSITORY_URL  ?= https://gitlab.com/sequoia-pgp/sequoia.git/

# Deploying documentation.
DOC_TARGET      ?= sequoia-pgp.org:docs.sequoia-pgp.org
RSYNC_FLAGS     ?=

# Tools.
CARGO           ?= cargo
GIT             ?= git
RSYNC           ?= rsync
RDFIND          ?= rdfind

sequoia:
	$(GIT) clone $(REPOSITORY_URL)

.PHONY: update
update:
	cd sequoia; $(GIT) fetch --all; $(GIT) checkout main ; $(GIT) pull

.PHONY: docs
docs: sequoia
	mkdir -p docs
	./build.bash sequoia docs.sequoia-pgp.org main
	cp fragments/htaccess docs.sequoia-pgp.org/.htaccess
	cp --recursive fragments/highlight.js docs.sequoia-pgp.org
	cp --recursive artwork/* docs.sequoia-pgp.org

.PHONY: deploy
deploy: docs
	$(RSYNC) $(RSYNC_FLAGS) --hard-links --compress -r docs.sequoia-pgp.org/* $(DOC_TARGET)
