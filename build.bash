#!/usr/bin/env bash

set -ue

SOURCE="$(realpath "$1")"
TARGET="$(realpath "$2")"

shift 2
if [ "${1:-}" ] ; then
    TAG="$1"
else
    TAG="main"
fi

BASE="$(pwd)"
CARGO="cargo"
CARGO_FLAGS=""

cd $SOURCE

echo "Documenting ${TAG}..."
git reset --hard
git checkout ${TAG}
rm -rf target/doc

RUSTDOCFLAGS="--html-in-header ${BASE}/fragments/highlight.js/9.12.0/inc.html \
              --html-before-content ${BASE}/fragments/before-content.html \
              --html-after-content ${BASE}/fragments/after-content.html \
              --extend-css ${BASE}/fragments/style.css" \
            ${CARGO} doc ${CARGO_FLAGS} --no-deps --all

mkdir -p ${TARGET}
rsync -a target/doc/* ${TARGET}
